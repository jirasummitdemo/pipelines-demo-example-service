package com.atlassian.phodder.example.application.service;

public interface ExampleService {

    /**
     *
     * @param key
     * @param value
     * @return
     */
    String createMap(String key, String value);

    /**
     *
     * @param key
     * @return
     */
    String getValue(String key);
}

package application;

import com.atlassian.phodder.example.application.ExampleServiceApplication;
import org.junit.Test;

public class ExampleServiceApplicationTest {
    @Test
    public void testRun() throws Exception {
        ExampleServiceApplication.main(new String[] {"dev.yml"});
    }
}